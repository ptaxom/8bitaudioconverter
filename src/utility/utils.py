from pydub import AudioSegment
from constants import Constants

class Utils:

    @staticmethod
    def convert_and_save_to_wav(path):
        # AudioSegment.ffmpeg = Constants.FFMPEG_PATH
        # AudioSegment.converter = Constants.FFMPEG_PATH
        sound = AudioSegment.from_mp3(path)
        sound.export(Constants.BUFFER_OBJECT, format="wav") 