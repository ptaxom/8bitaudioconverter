import pyaudio


class Constants:
    SAMPLE_NAME = "res/sample.mp3"
    BUFFER_OBJECT = "res/object.wav"
    FFMPEG_PATH = "D:/ffmpeg-4.1.3"
    OUTPUT_PATH = "res/out.wav"
    IMAGE_PATH = "res/ricardo.jpg"
    # VIDEO_PATH = "res/ricfull.mp4"
    VIDEO_PATH = "res/ric.mp4"
    # VIDEO_PATH = "res/nar2.mp4"
    LAB_PATH = "../../CPP/lab4/src/"

    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 16000
    CHUNK = 512
    START = 0
    N = 512