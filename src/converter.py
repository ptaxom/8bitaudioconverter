from pydub import AudioSegment
from utility.utils import Utils
from constants import Constants

import wave
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import wavfile as wav
from scipy.fftpack import fft
import pyaudio
import struct
import time
from tkinter import TclError

from PIL import Image


CHUNK = Constants.CHUNK
RATE = Constants.RATE

# Dependecy: sudo apt-get install libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0
# sudo apt-get install ffmpeg libav-tools

CHUNK = 4850



from pysine import sine
import time

import pygame

surf_width = 720
surf_height = 350

import moviepy
import cv2

to_plot = True
to_play = False
windowed = False

SAMPLE_LENGTH = 55 * 3 # X ms per sample
MAX_SEC_LENGTH = 30

min_freq, max_freq = 20, 16000


type_of_map = 2             # 0 - fast  1 - fast with plot  2 - long without plot

if type_of_map == 0:
    to_plot = False
    MAX_SEC_LENGTH = 5
if type_of_map == 1:
    to_plot = True
    MAX_SEC_LENGTH = 5
if type_of_map == 2:
    to_plot = False

class Frame:
    
    def __init__(self, pixels):
        self.pixels = pixels

    def get_pix(self, x, y):
        return self.pixels[x + y * 80]

    def to_byte_array(self):
        byte_format = []
        for y in range(0, 25):
            for x in range(0, 80):
                pix = self.pixels[y * 80 + x]
                bt = [x / 255 for x in pix]
                val = int(bt[0] * 4 + bt[1] * 2 + bt[2])
                byte_format.append(val)
        return byte_format
    
class Converter:

    def audio_convert():
        plot_amount = 2
        if windowed:
            plot_amount += 1
        fig, axs = plt.subplots(plot_amount, figsize=(15, 6))

        freqs = []

        input_obj = "res/{}.mp3".format("duhast")

        Utils.convert_and_save_to_wav(input_obj)
        wav_audio = AudioSegment.from_file(Constants.BUFFER_OBJECT, format="wav")
        parts = len(wav_audio) // SAMPLE_LENGTH
        parts = min(30 * 1000 // SAMPLE_LENGTH, parts)

        if to_play:
            p = pyaudio.PyAudio()
            wf = wave.open(Constants.BUFFER_OBJECT, 'rb')
            stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)

        if to_plot:
            fig.show()
        for i in range(0, parts):
            if i % (parts // 10) == 0:
                percent = i // (parts // 10)
                print("{}% done".format(percent * 10))
            current_sample = wav_audio[i * SAMPLE_LENGTH: (i + 1) * SAMPLE_LENGTH]
            samples = np.array(current_sample.get_array_of_samples()) + (2 ** 15) - 1 

            if to_play:
                data = wf.readframes(int(len(samples) * 2))
                stream.write(data)
                
            if windowed:
                w = np.hamming(len(samples))
                y_wfft = fft(samples * w)
            y_fft = fft(samples)
            xf = np.linspace(0, RATE, len(y_fft)) 
            
            max_freq_it = 0
            min_freq_it = -1
            for j in range(0, len(xf)):
                if xf[j] > min_freq:
                    min_freq_it = j
                    break
            max = -1
            parsed_freqs = y_fft
            if windowed:
                parsed_freqs = y_wfft

            for j in range(min_freq_it, len(parsed_freqs)):
                if parsed_freqs[j] > max:
                    max = parsed_freqs[j]
                    max_it = j
                if xf[j] > max_freq:
                    max_freq_it = j
                    break
            freqs.append(xf[max_it])
            if to_plot:
                # FFT plot
                x_axis, y_axis = xf[min_freq_it:max_freq_it], y_fft[min_freq_it:max_freq_it]
                CHUNK = len(y_axis)
                y_axis = np.abs(y_axis)  / (128 * CHUNK)
                axs[1].clear()
                axs[1].set_title("FFT")
                axs[1].plot(x_axis, y_axis)
                # Sample plot
                x_axis, y_axis = np.arange(len(samples)), samples
                axs[0].clear()
                axs[0].set_title("Amplitude")
                axs[0].plot(x_axis, y_axis)
                if windowed:
                    x_axis, y_axis = xf[min_freq_it:max_freq_it], y_wfft[min_freq_it:max_freq_it]
                    CHUNK = len(y_axis)
                    y_axis = np.abs(y_axis)  / (128 * CHUNK)
                    axs[2].clear()
                    axs[2].set_title("Windowed FFT")
                    axs[2].plot(x_axis, y_axis)
                fig.canvas.draw()
                fig.canvas.flush_events()
        if to_plot:
            fig.show()
        print(freqs)

        if to_play:
            stream.stop_stream()
            stream.close()
            p.terminate()
            


        bin_melodi = [int(freq) for freq in freqs]
        melodi = np.array([])
        for note in freqs:
            t = np.linspace(0, SAMPLE_LENGTH / 1000, SAMPLE_LENGTH / 1000 * 44100) 
            y = np.sin(note * 2 * np.pi * t)  #  Has frequency of 440Hz
            melodi = np.append(melodi, y)
        wav.write(Constants.OUTPUT_PATH, 44100, melodi)
        

        f = open("../../CPP/lab4/src/melodi.bin", "wb")
        bin_melodi.insert(0, len(bin_melodi))
        bin_melodi.insert(0, int(SAMPLE_LENGTH))
        byte_format = []
        for x in bin_melodi:
            byte_format.append(int(x % 256))
            byte_format.append(int(x / 256))
        binary_format = bytearray(byte_format)
        f.write(binary_format)
        string = ""
        for x in byte_format:
            string += chr(x)
        f.close()
        


    def set_tile(surface, x, y, color):
        tile_x = int(surf_width / 80)
        tile_y = int(surf_height / 25)
        for i in range(x * tile_x, (x + 1) * tile_x):
            for j in range(y * tile_y, (y + 1) * tile_y):
                surface.set_at((i, j), color)
 
    def to_clr(val):
        colors = [0, 0, 0]
        for i in range(0, 3):
            colors[2 - i] = (val >> i) & 1
        colors = [255 * i for i in colors]
        return tuple(colors)


    def get_color(image, x, y):
        width, height = image.size
        tile_x = int(width / 80)
        tile_y = int(height / 25)
        tile_square = tile_x * tile_y
        r0, g0, b0 = 0, 0, 0
        for i in range(x * tile_x, (x + 1) * tile_x):
            for j in range(y * tile_y, (y + 1) * tile_y):
                r, g, b = image.getpixel((i, j))
                r0, g0, b0 = r0 + r, g0 + g, b0 + b
        r0, g0, b0 = r0 / tile_square, g0 / tile_square, b0 / tile_square
        r0, g0, b0 = r0 // 128, g0 // 128, b0 // 128
        return Converter.to_clr(int(4 * r0 + 2 * g0 + b0))
   

    def split():
        vidcap = cv2.VideoCapture(Constants.VIDEO_PATH)
        success,image = vidcap.read()
        count = 0
        frame = 0
        while success:
            if count % 3 == 0:
                cv2.imwrite("res/frame_buffer/frame{}.jpg".format(frame), image)     # save frame as JPEG file      
                frame += 1
                if frame > 500:
                    break
            success,image = vidcap.read()
            count += 1
        return frame

    def draw_Frame(surf, frame):
        for i in range(0, 80):
            for j in range(0, 25):
                Converter.set_tile(surf, i, j, frame.get_pix(i, j))
                pygame.display.flip()

    def img_to_frame(img):
        rgb_im = img.convert('RGB')
        pixels = [] * (80 * 25)
        for j in range(0, 25):
            for i in range(0, 80):
                pixels.append(Converter.get_color(rgb_im, i, j))
        return Frame(pixels)

    def video_convert():
        frames_count = Converter.split()
        print(frames_count)
        # pygame.init()
        # surface = pygame.display.set_mode((surf_width, surf_height))
        frames = []

        for i in range(0, frames_count):
            print("Processing {} frame.".format(i))
            im = Image.open("res/frame_buffer/frame{}.jpg".format(i))
            frames.append(Converter.img_to_frame(im))
        
        
        print("preprocess ended")
        # while True:
        #     for frame in frames:
        #         Converter.draw_Frame(surface, frame)
                # time.sleep(0.165)
            
        f = open(Constants.LAB_PATH+"res/frame1.bin", "wb")
        video = []
        for frame in frames:
            video += frame.to_byte_array()
        lxx = len(frames)
        video = [lxx % 256, lxx // 256] + video
        # print(video)
        binary_format = bytearray(video)
        f.write(binary_format)
        f.close()

        print("ended")

        # while True:
        #     event = pygame.event.poll()
        #     if event.type == pygame.QUIT:
        #         break
        #     time.sleep(0.1)
